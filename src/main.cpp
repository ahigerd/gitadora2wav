#include "ifs/ifs.h"
#include "ifs/ifssequence.h"
#include "s2wcontext.h"
#include "riffwriter.h"
#include "synth/synthcontext.h"
#include "synth/channel.h"
#include "commandargs.h"
#include "tagmap.h"
#include <fstream>
#include <iostream>
#include <iomanip>
#include <algorithm>

int main(int argc, char** argv)
{
  CommandArgs args({
    { "help", "h", "", "Show this help text" },
    { "mute", "m", "parts", "Silence the selected channels" },
    { "solo", "s", "parts", "Only play the selected channels" },
    { "verbose", "v", "", "Output additional information about input files" },
    { "output", "o", "filename", "Set the output filename (default: add .wav to first IFS)" },
    { "", "", "input", "One or more IFS files" },
  });
  std::string argError = args.parse(argc, argv);
  if (!argError.empty()) {
    std::cerr << argError << std::endl;
    return 1;
  } else if (args.hasKey("help") || argc < 2) {
    std::cout << args.usageText(argv[0]) << std::endl;
    std::cout << std::endl;
    std::cout << "For mute or solo, specify one or more channels (e.g. \"dgbk\"):" << std::endl;
    std::cout << "\td  Drums" << std::endl;
    std::cout << "\tg  Guitar" << std::endl;
    std::cout << "\tb  Bass" << std::endl;
    std::cout << "\tk  Keyboard" << std::endl;
    std::cout << "\ts  Streamed backing track" << std::endl;
    return 0;
  } else if (!args.positional().size()) {
    std::cerr << argv[0] << ": at least one IFS is required" << std::endl;
    return 1;
  }

  S2WContext s2w;
  IFSSequence seq(&s2w);
  if (args.hasKey("mute")) {
    seq.setMutes(args.getString("mute"));
  }
  if (args.hasKey("solo")) {
    seq.setSolo(args.getString("solo"));
  }
  std::vector<std::string> positional = args.positional();
  for (const std::string& fn : args.positional()) {
    std::string paired = IFS::pairedFile(fn);
    if (std::find(positional.begin(), positional.end(), paired) == positional.end()) {
      positional.push_back(paired);
    }
  }
  for (const std::string& fn : positional) {
    std::ifstream file(fn, std::ios::in | std::ios::binary);
    IFS* ifs = new IFS(file);
    if (args.hasKey("verbose")) {
      ifs->manifest.dump();
    }
    seq.addIFS(ifs);
  }
  seq.load();
  if (!seq.numTracks()) {
    std::cerr << argv[0] << ": no playable tracks found, or all tracks muted" << std::endl;
  }

  for (const std::string& fn : positional) {
    std::string m3uPath = TagsM3U::relativeTo(fn);
    std::ifstream tags(m3uPath);
    if (!tags) {
      continue;
    }
    TagsM3U m3u(tags);
    int trackIndex = m3u.findTrack(fn);
    if (trackIndex >= 0) {
      std::cerr << "Tags found in " << m3uPath << ":" << std::endl;
      for (const auto& iter : m3u.allTags(trackIndex)) {
        std::cerr << iter.first << " = " << iter.second << std::endl;
      }
      break;
    }
  }

  SynthContext* ctx(seq.initContext());
  std::string filename = args.getString("output", args.positional()[0] + ".wav");
  std::cerr << "Writing " << (int(ctx->maximumTime() * 10) * .1) << " seconds to \"" << filename << "\"..." << std::endl;
  RiffWriter riff(seq.sampleRate, true);
  riff.open(filename == "-" ? "/dev/stdout" : filename);
  ctx->save(&riff);
  riff.close();
}
