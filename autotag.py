#!/usr/bin/python3

import os, sys

notetxt = '''
The .bin files in the root directory can be played using vgmstream (https://vgmstream.org/).
The .ifs files in the individual track directories can be played using gitadora2wav (https://bitbucket.org/ahigerd/gitadora2wav/downloads/).
'''

for folder in sys.argv[1:]:
    if folder[-1] == '/':
        folder = folder[:-1]
    if not os.path.isdir(folder):
        continue
    album = os.path.basename(folder).split('(')[0].strip()
    print(album)
    if '(' in folder:
        releasedate = os.path.basename(folder).split('(')[1].split(')')[0]
        year = releasedate.split('-')[0]
    else:
        releasedate = None
        year = None
    tracks = {}
    for track in os.listdir(folder):
        bin_order = []
        if os.path.exists(os.path.join(folder, '!tags.m3u')):
            bin_order = [line.strip() for line in open(os.path.join(folder, '!tags.m3u'), 'r') if line.strip() and line.strip()[0] != '#']
        tpath = os.path.join(folder, track)
        if not os.path.isdir(tpath):
            if tpath.endswith('.bin'):
                os.rename(tpath, tpath.replace('.bin', '.lbin'))
                tpath = tpath.replace('.bin', '.lbin')
            if tpath.endswith('.lbin'):
                if track in bin_order:
                    tracks[' %03d' % bin_order.index(track)] = track
                else:
                    tracks['0000_%s' % track] = track
            continue
        ifs = [fn for fn in os.listdir(tpath) if fn.endswith('.ifs')]
        if not ifs:
            continue
        print('\t', track)
        artist, title = track.split(' - ', maxsplit=1)
        trackno = ifs[0].split('_')[0].replace('m', '')
        tracks[trackno] = '%s/%s' % (track, ifs[0])
        with open(os.path.join(tpath, '!tags.m3u'), 'w') as m3u:
            m3u.write('# @ALBUM@ %s\n' % album)
            m3u.write('# @ALBUMARTIST@ %s\n' % 'Konami')
            if releasedate:
                m3u.write('# @DATE@ %s\n' % releasedate)
            if year:
                m3u.write('# @YEAR@ %s\n' % year)
            m3u.write('# @ARTIST@ %s\n' % artist)
            m3u.write('# @TITLE@ %s\n' % title)
            m3u.write('# @TRACK@ %s\n' % trackno)
            for fn in ifs:
                m3u.write('%s\n' % fn)
    with open(os.path.join(folder, 'note.txt'), 'w') as notefile:
        notefile.write('Game: %s\nPublisher: Konami\n%s' % (os.path.basename(folder).replace('(Konami)', ''), notetxt))
    with open(os.path.join(folder, '%s.m3u8' % album), 'w') as playlist:
        for order, track in sorted(tracks.items()):
            playlist.write('%s\n' % track)
